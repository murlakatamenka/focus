# Fokus

Fokus is simple pomodoro app writen with Kirigami and QtQuick.

![](screens/main.png) ![](screens/break.png)

## Build

mkdir build  
cd build  
cmake -DKDE_INSTALL_USE_QT_SYS_PATHS=ON  ..  
make  
(sudo) make install  
