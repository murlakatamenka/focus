#ifndef NOTIFY_H
#define NOTIFY_H

#include <QObject>
#include <QDebug>
#include <klocalizedstring.h>
#include <knotifyconfig.h>
#include <knotification.h>

class Notify : public QObject
{
    Q_OBJECT

public:
    explicit Notify(QObject *parent = nullptr);
    ~Notify();
public slots:
    void notify(int id);
};

#endif // NOTIFY_H