#include "notify.h"

Notify::Notify(QObject *parent) : QObject(parent)
{
}

Notify::~Notify()
{
}

void Notify::notify(int id)
{
    switch (id)
    {
    case 1:
        KNotification::event(KNotification::StandardEvent::Notification, "Focus", "Focus on your work!",
                             "ktimer", nullptr, KNotification::CloseOnTimeout);
        break;
    case 2:
        KNotification::event(KNotification::StandardEvent::Notification, "Short Break", "Go for a walk.",
                             "ktimer", nullptr, KNotification::CloseOnTimeout);
        break;
    case 3:
        KNotification::event(KNotification::StandardEvent::Notification, "Long Break", "Take a long break!",
                             "ktimer", nullptr, KNotification::CloseOnTimeout);
        break;
    case 4:
        KNotification::event(KNotification::StandardEvent::Notification, "Foucs", "End of time.",
                             "ktimer", nullptr, KNotification::CloseOnTimeout);
        break;
    default:
        break;
    }
}
