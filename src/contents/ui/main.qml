import QtQuick 2.7
import org.kde.kirigami 2.5 as Kirigami
import QtQuick.Controls 2.5 as Controls
import CustomControls 1.0
import NotifyComponents 1.0

Kirigami.ApplicationWindow {
    id: root
    width: 220
    height: 200

    minimumWidth: 200
    minimumHeight: 200

    pageStack.initialPage: mainPageComponent

    Notify {
        id: notify
    }

    Component {
        id: mainPageComponent

        Kirigami.Page {
            title: qsTr("Timer")
            property var min: 25
            property var sec: 0
            property var stateVal: 1
            property var maxTime: 1500
            property var currTime: 1500

            actions {
                left:Kirigami.Action {
                    id: skipBtn
                    icon.name: "media-skip-forward"
                    text: qsTr("Skip")
                    onTriggered: {
                        skip()
                    }
                }
                main:Kirigami.Action {
                    id: sessionBtn
                    icon.name: "media-playback-start"
                    text: qsTr("Start")
                    onTriggered: {
                        if(sessionBtn.text == qsTr("Start")) {
                            start()
                        } else {
                            pause()
                        }
                    }
                }

                right:Kirigami.Action {
                    id: breakBtn
                    icon.name: "media-playback-stop"
                    text: qsTr("Stop")
                    onTriggered: {
                        stop()
                    }
                }
            }

            function start(){
                notify.notify(stateVal);
                textTimer.start()
                sessionBtn.text = qsTr("Pause")
                sessionBtn.icon.name= "media-playback-pause"
            }

            function pause() {
                textTimer.stop()
                sessionBtn.text = qsTr("Start")
                sessionBtn.icon.name= "media-playback-start"
            }

            function skip() {
                nextState()
                resetTime()
            }

            function stop() {
                textTimer.stop()
                resetTime()
                stateVal = 1
                sessionBtn.text = qsTr("Start")
                sessionBtn.icon.name= "media-playback-start"
            }

            function end() {
                 textTimer.stop()
                 notify.notify(4);
                 sessionBtn.text = qsTr("Start")
                 sessionBtn.icon.name= "media-playback-start"
                 nextState()
                 resetTime()
            }

            function enableButtons() {
                focusBtn.enabled = true
                breakBtn.enabled = true
            }

            function disableButtons() {
                focusBtn.enabled = false
                breakBtn.enabled = false
            }

            function resetTime() {
                sec = 0
                switch(stateVal) {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                        min = 25
                        currTime = 1500
                        maxTime = 1500
                        status.text = qsTr("focus")
                        break;
                    case 2:
                    case 4:
                    case 6:
                        min = 5
                        currTime = 300
                        maxTime = 300
                        status.text = qsTr("short break")
                        break;
                    case 8:
                        min = 20
                        currTime = 1200
                        maxTime = 1200
                        status.text = qsTr("long break")
                        break;
                }

                time.update()
            }

            function nextState() {
                if(stateVal < 8) {
                    stateVal++
                } else {
                    stateVal = 1
                }
            }

            Timer {
                id: textTimer
                interval: 1000
                repeat: true
                running: false
                triggeredOnStart: false
                onTriggered: time.set()
            }


            Rectangle {
                anchors.fill: parent
                color: Kirigami.Theme.backgroundColor

                RadialBar {
                    id: radialBar
                    width: Math.min(parent.width, parent.height)
                    height: width
                    anchors.centerIn: parent;
                    dialType: RadialBar.FullDial
                    progressColor: Kirigami.Theme.activeTextColor
                    foregroundColor: Kirigami.Theme.backgroundColor
                    backgroundColor: Kirigami.Theme.viewBackgroundColor
                    dialWidth: width/20
                    startAngle: 180
                    minValue: 0
                    maxValue: maxTime
                    value: currTime
                    showText: false
                }

                Rectangle {
                    anchors.centerIn: parent;
                    height: time.height 
                    color: Kirigami.Theme.backgroundColor

                    Controls.Label {
                        id: time
                        text: formatNumberLength(min,2) + ":" + formatNumberLength(sec,2)
                        font.pointSize: radialBar.width/6
                        anchors.horizontalCenter: parent.horizontalCenter

                        function set() {
                            if(sec == 0) {
                                min--
                                sec = 59
                            } else {
                                sec--
                            }

                            currTime--

                            if(currTime == 0) {
                                end()
                            }

                            time.update()
                        }

                        function update() {
                            time.text = formatNumberLength(min,2) + ":" + formatNumberLength(sec,2)
                        }

                        function formatNumberLength(num, length) {
                            var r = "" + num;
                            while (r.length < length) {
                                r = "0" + r;
                            }
                            return r;
                        }
                    }


                    Controls.PageIndicator {
                        id: pageIndicator
                        count: 4
                        currentIndex: (stateVal - 1)/2

                        anchors.bottom: time.top
                        anchors.horizontalCenter: parent.horizontalCenter

                        spacing: radialBar.width/25
                        delegate: Rectangle {
                            implicitWidth: radialBar.width/25
                            implicitHeight: width
                            radius: width / 2
                            color: Kirigami.Theme.negativeTextColor

                            opacity: index === pageIndicator.currentIndex ? 0.95 : 0.3

                            Behavior on opacity {
                                OpacityAnimator {
                                    duration: 100
                                }
                            }
                        }
                    }

                    Controls.Label {
                        id: status
                        text: qsTr("focus")
                        font.pointSize: radialBar.width/15
                        anchors.top: time.bottom
                        anchors.horizontalCenter: parent.horizontalCenter
                    }
                }
            }
        }
    }
}
